import math
import cv2
import numpy as np


# Helper function: ensures num is in the range [0, 255]
def ensure_num_between_0_and_255(num):
    if num > 255:
        num = 255
    if num < 0:
        num = 0
    return num


# Helper function: ensures num is in the range [0, 1]
def ensure_num_between_0_and_1(num):
    if num > 1:
        num = 1
    if num < 0:
        num = 0
    return num


# Helper function: ensures angle is in range [0, 2pi]
def ensure_angle_between_0_and_2pi(angle):
    while angle > 2 * np.pi:
        angle -= 2 * np.pi
    while angle < 0:
        angle += 2 * np.pi
    return angle


# Helper function: applies transformation function T to image img
def apply_tfrm_curve(img, T):
    ret_img = img.copy()
    height = ret_img.shape[0]
    width = ret_img.shape[1]

    for row in range(height):
        for col in range(width):
            intensity = ret_img[row][col]
            ret_img[row][col] = T[intensity]
    return ret_img


# Helper function: converts image from unsigned chars to floats
def rgb_bytes_to_floats(img):
    height = img.shape[0]
    width = img.shape[1]
    ret_img = np.zeros((height, width), dtype=(float, 3))

    for row in range(height):
        for col in range(width):
            rgb = img[row, col]

            ret_img[row, col][0] = float(rgb[0]) / 255
            ret_img[row, col][1] = float(rgb[1]) / 255
            ret_img[row, col][2] = float(rgb[2]) / 255

    return ret_img


# Helper function: converts image from floats to unsigned chars
def rgb_floats_to_bytes(img):
    height = img.shape[0]
    width = img.shape[1]
    ret_img = np.zeros((height, width), dtype=(np.uint8, 3))

    for row in range(height):
        for col in range(width):
            rgb = img[row, col]

            ret_img[row, col][0] = ensure_num_between_0_and_255(np.round(rgb[0] * 255))
            ret_img[row, col][1] = ensure_num_between_0_and_255(np.round(rgb[1] * 255))
            ret_img[row, col][2] = ensure_num_between_0_and_255(np.round(rgb[2] * 255))

    return ret_img


# Section 3.1 Question 1
def calculate_theta(b, g, r):
    numerator = 0.5 * (2 * r - g - b)
    denominator = math.sqrt((r - g) * (r - g) + (r - b) * (g - b))

    if numerator / denominator >= 1:
        return np.arccos(1)

    if numerator / denominator <= -1:
        return np.arccos(-1)

    return np.arccos(numerator / denominator)


def rgb_to_hsi(img):
    input_img = img.copy()
    height = input_img.shape[0]
    width = input_img.shape[1]
    ret_img = np.zeros((height, width), dtype=(float, 3))

    if input_img.dtype == np.uint8:
        input_img = rgb_bytes_to_floats(input_img)

    for row in range(height):
        for col in range(width):
            # opencv uses BGR order instead of RGB order
            bgr = input_img[row, col]

            b = bgr[0]
            g = bgr[1]
            r = bgr[2]
            c_min = bgr.min()

            if r == g and g == b:
                H = 0
            elif b <= g:
                H = calculate_theta(b, g, r)
            else:
                H = 2 * np.pi - calculate_theta(b, g, r)

            S = 1 - (3 / (r + g + b)) * c_min
            I = (r + g + b) / 3

            ret_img[row, col][0] = H
            ret_img[row, col][1] = S
            ret_img[row, col][2] = I

    return ret_img


# Section 3.1 Question 2
def hsi_to_rgb(img):
    height = img.shape[0]
    width = img.shape[1]
    ret_img = np.zeros((height, width), dtype=(float, 3))

    for row in range(height):
        for col in range(width):
            HSI = img[row, col]
            H = HSI[0]
            S = HSI[1]
            I = HSI[2]

            if 0 <= H < 2 * np.pi / 3:
                b = I * (1 - S)
                r = I * (1 + S * np.cos(H) / np.cos(np.pi / 3 - H))
                g = 3 * I - (r + b)
            elif 2 * np.pi / 3 <= H < 4 * np.pi / 3:
                H = H - 2 * np.pi / 3
                r = I * (1 - S)
                g = I * (1 + S * np.cos(H) / np.cos(np.pi / 3 - H))
                b = 3 * I - (r + g)
            else:
                H = H - 4 * np.pi / 3
                g = I * (1 - S)
                b = I * (1 + S * np.cos(H) / np.cos(np.pi / 3 - H))
                r = 3 * I - (g + b)

            ret_img[row, col][0] = b
            ret_img[row, col][1] = g
            ret_img[row, col][2] = r

    return ret_img


input_image = cv2.imread('Images/peppers.png')
hsi_version = rgb_to_hsi(input_image)
rgb_version = rgb_floats_to_bytes(hsi_to_rgb(hsi_version))
cv2.imwrite('Images/peppers_identical_hsi.png', np.hstack([input_image, rgb_version]))

input_image = cv2.imread('Images/tree.png')
hsi_version = rgb_to_hsi(input_image)
rgb_version = rgb_floats_to_bytes(hsi_to_rgb(hsi_version))
cv2.imwrite('Images/tree_identical_hsi.png', np.hstack([input_image, rgb_version]))


# Section 3.1 Question 3
def rgb_to_ycbcr(img):
    height = img.shape[0]
    width = img.shape[1]

    if img.dtype == np.uint8:
        inputs_are_normalized_floats = False
        ret_img = np.zeros((height, width), dtype=(np.uint8, 3))
    else:
        inputs_are_normalized_floats = True
        ret_img = np.zeros((height, width), dtype=(float, 3))

    for row in range(height):
        for col in range(width):
            # opencv uses BGR order instead of RGB order
            bgr = img[row, col]

            transform_matrix = np.array([
                [0.299, 0.587, 0.114],
                [-0.1687, -0.3313, 0.5],
                [0.5, -0.4187, -0.0813]
            ])

            rgb_matrix = np.array([
                [bgr[2]],  # red
                [bgr[1]],  # green
                [bgr[0]]  # blue
            ])

            if inputs_are_normalized_floats:
                offset_matrix = np.array([
                    [0],
                    [0],
                    [0]
                ])
            else:
                offset_matrix = np.array([
                    [0],
                    [128],
                    [128]
                ])

            ycbcr_matrix = transform_matrix @ rgb_matrix + offset_matrix

            ret_img[row, col][0] = ycbcr_matrix[0] if inputs_are_normalized_floats else ensure_num_between_0_and_255(
                np.round(ycbcr_matrix[0]))
            ret_img[row, col][1] = ycbcr_matrix[1] if inputs_are_normalized_floats else ensure_num_between_0_and_255(
                np.round(ycbcr_matrix[1]))
            ret_img[row, col][2] = ycbcr_matrix[2] if inputs_are_normalized_floats else ensure_num_between_0_and_255(
                np.round(ycbcr_matrix[2]))

    return ret_img


# Section 3.1 Question 4
def ycbcr_to_rgb(img):
    height = img.shape[0]
    width = img.shape[1]

    if img.dtype == np.uint8:
        inputs_are_normalized_floats = False
        ret_img = np.zeros((height, width), dtype=(np.uint8, 3))
    else:
        inputs_are_normalized_floats = True
        ret_img = np.zeros((height, width), dtype=(float, 3))

    for row in range(height):
        for col in range(width):
            # opencv uses BGR order instead of RGB order
            ycbcr = img[row, col]

            transform_matrix = np.array([
                [1, 0, 1.402],
                [1, -0.34414, -0.71414],
                [1, 1.772, 0]
            ])

            ycbcr_matrix = np.array([
                [ycbcr[0]],  # Y'
                [ycbcr[1]],  # Cb
                [ycbcr[2]]  # Cr
            ])

            if inputs_are_normalized_floats:
                offset_matrix = np.array([
                    [0],
                    [0],
                    [0]
                ])
            else:
                offset_matrix = np.array([
                    [0],
                    [128],
                    [128]
                ])

            rgb_matrix = transform_matrix @ (ycbcr_matrix - offset_matrix)

            ret_img[row, col][0] = rgb_matrix[2] if inputs_are_normalized_floats else ensure_num_between_0_and_255(
                np.round(rgb_matrix[2]))
            ret_img[row, col][1] = rgb_matrix[1] if inputs_are_normalized_floats else ensure_num_between_0_and_255(
                np.round(rgb_matrix[1]))
            ret_img[row, col][2] = rgb_matrix[0] if inputs_are_normalized_floats else ensure_num_between_0_and_255(
                np.round(rgb_matrix[0]))

    return ret_img


input_image = cv2.imread('Images/peppers.png')
ycbcr_version = rgb_to_ycbcr(input_image)
rgb_version = ycbcr_to_rgb(ycbcr_version)
cv2.imwrite('Images/peppers_identical_ycbcr.png', np.hstack([input_image, rgb_version]))

input_image = cv2.imread('Images/tree.png')
ycbcr_version = rgb_to_ycbcr(input_image)
rgb_version = ycbcr_to_rgb(ycbcr_version)
cv2.imwrite('Images/tree_identical_ycbcr.png', np.hstack([input_image, rgb_version]))


# Section 3.2 Question 1
# hue_angle is in radians
def change_hue(img, hue_angle):
    ret_img = img.copy()
    height = ret_img.shape[0]
    width = ret_img.shape[1]

    for row in range(height):
        for col in range(width):
            old_hue = ret_img[row, col][0]
            new_hue = ensure_angle_between_0_and_2pi(old_hue + hue_angle)
            ret_img[row, col][0] = new_hue

    return ret_img


input_image = cv2.imread('Images/peppers.png')
hsi_version = rgb_to_hsi(input_image)
hsi_version = change_hue(hsi_version, np.pi)
rgb_version = rgb_floats_to_bytes(hsi_to_rgb(hsi_version))
cv2.imwrite('Images/peppers_changed_hue.png', np.hstack([input_image, rgb_version]))

input_image = cv2.imread('Images/tree.png')
hsi_version = rgb_to_hsi(input_image)
hsi_version = change_hue(hsi_version, np.pi / 2)
rgb_version = rgb_floats_to_bytes(hsi_to_rgb(hsi_version))
cv2.imwrite('Images/tree_changed_hue.png', np.hstack([input_image, rgb_version]))


# Section 3.2 Question 2
def change_saturation(img, sat):
    ret_img = img.copy()
    height = ret_img.shape[0]
    width = ret_img.shape[1]

    for row in range(height):
        for col in range(width):
            old_saturation = ret_img[row, col][1]
            new_saturation = ensure_num_between_0_and_1(old_saturation + sat)
            ret_img[row, col][1] = new_saturation

    return ret_img


input_image = cv2.imread('Images/peppers.png')
hsi_version = rgb_to_hsi(input_image)
hsi_version = change_saturation(hsi_version, -0.3)
rgb_version = rgb_floats_to_bytes(hsi_to_rgb(hsi_version))
cv2.imwrite('Images/peppers_changed_saturation.png', np.hstack([input_image, rgb_version]))

input_image = cv2.imread('Images/tree.png')
hsi_version = rgb_to_hsi(input_image)
hsi_version = change_saturation(hsi_version, -0.2)
rgb_version = rgb_floats_to_bytes(hsi_to_rgb(hsi_version))
cv2.imwrite('Images/tree_changed_saturation.png', np.hstack([input_image, rgb_version]))


# Section 3.2 Question 3
def apply_point_tfrm(in_img, C, B):
    ret_img = in_img.copy()
    height = ret_img.shape[0]
    width = ret_img.shape[1]

    for row in range(height):
        for col in range(width):
            ret_img[row][col][0] = ensure_num_between_0_and_255(C * ret_img[row][col][0] + B)
            ret_img[row][col][1] = ensure_num_between_0_and_255(C * ret_img[row][col][1] + B)
            ret_img[row][col][2] = ensure_num_between_0_and_255(C * ret_img[row][col][2] + B)
    return ret_img


input_image = cv2.imread('Images/peppers.png')
output_image = apply_point_tfrm(input_image, 0.2, 50)
cv2.imwrite('Images/peppers_apply_point_tfrm.png', np.hstack([input_image, output_image]))

input_image = cv2.imread('Images/tree.png')
output_image = apply_point_tfrm(input_image, 1.2, 0)
cv2.imwrite('Images/tree_apply_point_tfrm.png', np.hstack([input_image, output_image]))


# Section 3.2 Question 4

# Section 3.2 Question 5
def generate_transform_function_histogram_equalize(img):
    height = img.shape[0]
    width = img.shape[1]

    pdf_original = [0 for x in range(256)]
    T = [0 for x in range(256)]

    # At each intensity level, get the number of pixels
    for row in range(height):
        for col in range(width):
            pdf_original[img[row][col]] += 1

    # At each intensity level, divide the number of pixels by the total amount of pixels
    for i in range(256):
        pdf_original[i] /= (width * height)

    # Generate the transformation function at i by (summing the pdf from 0 to i) * 255
    for i in range(256):  # from i = 0 to i = 255
        for j in range(0, i + 1):  # from j = 0 to j=i
            T[i] += 255 * pdf_original[j]

    # Clean up transformation function values
    for i in range(256):
        pdf_original[i] = ensure_num_between_0_and_255(np.round(pdf_original[i]))

    return T


def histogram_equalize(img):
    ret_img = img.copy()
    b = ret_img[:, :, 0]
    g = ret_img[:, :, 1]
    r = ret_img[:, :, 2]
    b = apply_tfrm_curve(b, generate_transform_function_histogram_equalize(b))
    g = apply_tfrm_curve(g, generate_transform_function_histogram_equalize(g))
    r = apply_tfrm_curve(r, generate_transform_function_histogram_equalize(r))
    ret_img[:, :, 0] = b
    ret_img[:, :, 1] = g
    ret_img[:, :, 2] = r
    return ret_img


input_image = cv2.imread('Images/peppers.png')
output_image = histogram_equalize(input_image)
cv2.imwrite('Images/peppers_histogram_equalized.png', np.hstack([input_image, output_image]))

input_image = cv2.imread('Images/tree.png')
output_image = histogram_equalize(input_image)
cv2.imwrite('Images/tree_histogram_equalized.png', np.hstack([input_image, output_image]))


# Section 3.2 Question 6
def histogram_equalize2(img):
    ret_img = img.copy()
    ret_img = rgb_to_ycbcr(ret_img)
    Y = ret_img[:, :, 0]
    Y = apply_tfrm_curve(Y, generate_transform_function_histogram_equalize(Y))
    ret_img[:, :, 0] = Y
    ret_img = ycbcr_to_rgb(ret_img)
    return ret_img


input_image = cv2.imread('Images/peppers.png')
output_image = histogram_equalize2(input_image)
cv2.imwrite('Images/peppers_histogram_equalized2.png', np.hstack([input_image, output_image]))

input_image = cv2.imread('Images/tree.png')
output_image = histogram_equalize2(input_image)
cv2.imwrite('Images/tree_histogram_equalized2.png', np.hstack([input_image, output_image]))

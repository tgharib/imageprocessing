import math
import cv2
import numpy as np


# Helper function: ensures pixel intensities stay in the range 0-255
def keep_8_bits(num):
    if num > 255:
        num = 255
    if num < 0:
        num = 0
    return num


# Helper function: applies transformation function T to image img
def apply_tfrm_curve(img, T):
    ret_img = img.copy()
    height = ret_img.shape[0]
    width = ret_img.shape[1]

    for row in range(height):
        for col in range(width):
            intensity = ret_img[row][col]
            ret_img[row][col] = T[intensity]
    return ret_img


# Helper function: applies piecewise contrast stretching
def contrast_piecewise(in_img, a, b):
    ret_img = in_img.copy()
    height = ret_img.shape[0]
    width = ret_img.shape[1]
    r_1 = a[0]
    s_1 = a[1]
    r_2 = b[0]
    s_2 = b[1]

    line_1_slope = s_1 / r_1 if r_1 else 0
    line_2_slope = (s_2 - s_1) / (r_2 - r_1) if (r_2 - r_1) else 0
    line_3_slope = (255 - s_2) / (255 - r_2) if (255 - r_2) else 0
    line_2_y_intercept = s_1 - line_2_slope * r_1
    line_3_y_intercept = s_2 - line_3_slope * r_2

    for row in range(height):
        for col in range(width):
            intensity = ret_img[row][col]
            if intensity < r_1:
                ret_img[row][col] = keep_8_bits(np.round(line_1_slope * intensity))
            elif intensity > r_2:
                ret_img[row][col] = keep_8_bits(np.round(line_3_slope * intensity + line_3_y_intercept))
            else:
                ret_img[row][col] = keep_8_bits(np.round(line_2_slope * intensity + line_2_y_intercept))
    return ret_img


# Section 2.1 Question 1
def generate_transform_function_histogram_equalize(img):
    height = img.shape[0]
    width = img.shape[1]

    pdf_original = [0 for x in range(256)]
    T = [0 for x in range(256)]

    # At each intensity level, get the number of pixels
    for row in range(height):
        for col in range(width):
            pdf_original[img[row][col]] += 1

    # At each intensity level, divide the number of pixels by the total amount of pixels
    for i in range(256):
        pdf_original[i] /= (width * height)

    # Generate the transformation function at i by (summing the pdf from 0 to i) * 255
    for i in range(256):  # from i = 0 to i = 255
        for j in range(0, i + 1):  # from j = 0 to j=i
            T[i] += 255 * pdf_original[j]

    # Clean up transformation function values
    for i in range(256):
        pdf_original[i] = keep_8_bits(np.round(pdf_original[i]))

    return T


def histogram_equalize(img):
    ret_img = img.copy()
    ret_img = apply_tfrm_curve(ret_img, generate_transform_function_histogram_equalize(ret_img))
    return ret_img


# Section 2.1 Question 2
# Helper function: applies transformation function T to a pixel in image img and returns intensity
def apply_tfrm_curve_to_pixel(img, T, row, col):
    return T[img[row, col]]


def adaptive_histogram(img, H, W):
    # Ensure the window has a center pixel
    assert (H % 2) != 0
    assert (W % 2) != 0

    orig_img = img.copy()

    ret_img = img.copy()
    height = img.shape[0]
    width = img.shape[1]

    # Apply histogram equalization for each WxH neighbourhood
    for row in range(math.ceil(-H / 2), height - math.ceil(H / 2)):
        for col in range(math.ceil(-W / 2), width - math.ceil(W / 2)):
            cropped_window = orig_img[row if row >= 0 else 0:row + H, col if col >= 0 else 0:col + W]
            if row > height - H:
                cropped_window = cv2.copyMakeBorder(cropped_window, 0, row - (height - H), 0, 0, cv2.BORDER_REFLECT)
            if col > width - W:
                cropped_window = cv2.copyMakeBorder(cropped_window, 0, 0, 0, col - (width - W), cv2.BORDER_REFLECT)
            if row < 0:
                cropped_window = cv2.copyMakeBorder(cropped_window, -row, 0, 0, 0, cv2.BORDER_REFLECT)
            if col < 0:
                cropped_window = cv2.copyMakeBorder(cropped_window, 0, 0, -col, 0, cv2.BORDER_REFLECT)

            cropped_window_height = cropped_window.shape[0]
            cropped_window_width = cropped_window.shape[1]
            cropped_window_center_vertical = math.floor(cropped_window_height / 2)
            cropped_window_center_horizontal = math.floor(cropped_window_width / 2)

            new_intensity = apply_tfrm_curve_to_pixel(cropped_window,
                                                      generate_transform_function_histogram_equalize(
                                                          cropped_window),
                                                      cropped_window_center_vertical,
                                                      cropped_window_center_horizontal)

            ret_img[row + cropped_window_center_vertical, col + cropped_window_center_horizontal] = new_intensity

    return ret_img


# Section 2.1 Question 3
def unsharp_mask(img, r, k):
    ret_img = img.copy()
    height = img.shape[0]
    width = img.shape[1]

    lpf_img = cv2.filter2D(ret_img, -1, np.ones((r, r)) / (r * r))

    for row in range(height):
        for col in range(width):
            ret_img[row][col] = keep_8_bits(np.round((k + 1) * ret_img[row][col] - k * lpf_img[row][col]))

    return ret_img


# Section 2.1 Question 4
def laplacian_sharpen(img, k):
    ret_img = img.copy()
    height = img.shape[0]
    width = img.shape[1]

    laplacian_img = cv2.filter2D(ret_img, -1, np.array([[0, 1, 0], [1, -4, 1], [0, 1, 0]]))

    for row in range(height):
        for col in range(width):
            ret_img[row][col] = keep_8_bits(np.round(ret_img[row][col] - k * laplacian_img[row][col]))

    return ret_img


# Section 2.2 Question 1
input_image = cv2.imread('Images/testing/contrast/207056.jpg', 0)  # load in grayscale
output_image = histogram_equalize(input_image)
cv2.imwrite('Images/testing/contrast/207056_global_histogram.png', output_image)

input_image = cv2.imread('Images/testing/contrast/7.2.01-small.png', 0)  # load in grayscale
output_image = histogram_equalize(input_image)
cv2.imwrite('Images/testing/contrast/7.2.01-small_global_histogram.png', output_image)

# Section 2.2. Question 2
input_image = cv2.imread('Images/testing/contrast/207056.jpg', 0)  # load in grayscale
output_image = adaptive_histogram(input_image, 101, 101)
cv2.imwrite('Images/testing/contrast/207056_local_histogram.png', output_image)

input_image = cv2.imread('Images/testing/contrast/7.2.01-small.png', 0)  # load in grayscale
output_image = adaptive_histogram(input_image, 101, 101)
cv2.imwrite('Images/testing/contrast/7.2.01-small_local_histogram.png', output_image)

# Section 2.2 Question 3
input_image = cv2.imread('Images/testing/sharpen/7.2.01-small.png', 0)  # load in grayscale
output_image = unsharp_mask(input_image, 21, 0.5)
cv2.imwrite('Images/testing/sharpen/7.2.01-small_unsharp_mask.png', output_image)

input_image = cv2.imread('Images/testing/sharpen/digital_orca_blurred.png', 0)  # load in grayscale
output_image = unsharp_mask(input_image, 21, 0.5)
cv2.imwrite('Images/testing/sharpen/digital_orca_blurred_unsharp_mask.png', output_image)

# Section 2.2 Question 4
input_image = cv2.imread('Images/testing/sharpen/7.2.01-small.png', 0)  # load in grayscale
output_image = laplacian_sharpen(input_image, 1)
cv2.imwrite('Images/testing/sharpen/7.2.01-small_laplacian_sharpen.png', output_image)

input_image = cv2.imread('Images/testing/sharpen/digital_orca_blurred.png', 0)  # load in grayscale
output_image = laplacian_sharpen(input_image, 1)
cv2.imwrite('Images/testing/sharpen/digital_orca_blurred_laplacian_sharpen.png', output_image)

# Section 2.3 (noiseadditive.png)
input_image = cv2.imread('Images/enhance/noise_additive.png', 0)  # load in grayscale
lpf_image = cv2.filter2D(input_image, -1, np.ones((3, 3)) / (3 * 3))
hst_image = histogram_equalize(lpf_image)
sharpen_image = laplacian_sharpen(hst_image, 0.1)
cv2.imwrite('Images/enhance/noise_additive_enhanced.png', sharpen_image)

# Section 2.3 (noisemultiplicative.png)
input_image = cv2.imread('Images/enhance/noise_multiplicative.png', 0)  # load in grayscale
lpf_image = cv2.filter2D(input_image, -1, np.ones((3, 3)) / (3 * 3))
hst_image = histogram_equalize(lpf_image)
sharpen_image = laplacian_sharpen(hst_image, 0.1)
cv2.imwrite('Images/enhance/noise_multiplicative_enhanced.png', sharpen_image)

# Section 2.3 (noiseimpulsive.png)
input_image = cv2.imread('Images/enhance/noise_impulsive.png', 0)  # load in grayscale
lpf_image = cv2.medianBlur(input_image, 3)
hst_image = histogram_equalize(lpf_image)
sharpen_image = laplacian_sharpen(hst_image, 0.1)
cv2.imwrite('Images/enhance/noise_impulsive_enhanced.png', sharpen_image)

# Section 2.3 (snowglobe.png)
input_image = cv2.imread('Images/enhance/snowglobe.png', 0)  # load in grayscale
lpf_image = cv2.filter2D(input_image, -1, np.ones((3, 3)) / (3 * 3))
hst_image = contrast_piecewise(lpf_image, [10, 0], [150, 255])
sharpen_image = laplacian_sharpen(hst_image, 0.1)
cv2.imwrite('Images/enhance/snowglobe_enhanced.png', sharpen_image)

# https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_histograms/py_histogram_begins/py_histogram_begins.html#plotting-histograms
# from matplotlib import pyplot as plt
# plt.hist(input_image.ravel(), 256, [0, 256])
# plt.show()
# def show_images_and_wait(*img):
#     cv2.imshow('image window', np.hstack(img))
#     cv2.waitKey(0)
#     cv2.destroyAllWindows()

import cv2
import numpy as np


# Helper function
def keep_8_bits(num):
    if num > 255:
        num = 255
    if num < 0:
        num = 0
    return num


# Section 2.1 Question 1
def apply_point_tfrm(in_img, C, B):
    ret_img = in_img.copy()
    height = ret_img.shape[0]
    width = ret_img.shape[1]

    for row in range(height):
        for col in range(width):
            ret_img[row][col] = keep_8_bits(C * ret_img[row][col] + B)
    return ret_img


input_image = cv2.imread('Images/Section2.1 - Q1/183087.jpg', 0)  # load in grayscale
output_image = apply_point_tfrm(input_image, 1, -70)
cv2.imwrite('Images/Section2.1 - Q1/183087_output.png', output_image)

input_image = cv2.imread('Images/Section2.1 - Q1/207056.jpg', 0)  # load in grayscale
output_image = apply_point_tfrm(input_image, 2, 0)
cv2.imwrite('Images/Section2.1 - Q1/207056_output.png', output_image)


# Section 2.1 Question 2
def apply_mask(img_a, img_b, img_mask):
    ret_img = img_mask.copy()
    height = ret_img.shape[0]
    width = ret_img.shape[1]

    for row in range(height):
        for col in range(width):
            if ret_img[row][col] > 0:
                ret_img[row][col] = img_a[row][col]
            else:
                ret_img[row][col] = img_b[row][col]
    return ret_img


bridge = cv2.imread('Images/Section2.1 - Q2/bridge.png', 0)  # load in grayscale
fish = cv2.imread('Images/Section2.1 - Q2/fish.png', 0)  # load in grayscale
mask = cv2.imread('Images/Section2.1 - Q2/mask.png', 0)  # load in grayscale
output_image = apply_mask(bridge, fish, mask)
cv2.imwrite('Images/Section2.1 - Q2/bridge_on_fish_output.png', output_image)
output_image = apply_mask(fish, bridge, mask)
cv2.imwrite('Images/Section2.1 - Q2/fish_on_bridge_output.png', output_image)


# Section 2.1 Question 3
def average_images(img_list):
    height = img_list[0].shape[0]
    width = img_list[0].shape[1]
    ret_img = np.zeros((height, width), np.uint8)

    for row in range(height):
        for col in range(width):
            pixel = np.double(0)
            for img in img_list:
                pixel += img[row][col]
            pixel /= len(img_list)
            ret_img[row][col] = keep_8_bits(np.round(pixel))
    return ret_img


snowglobe1 = cv2.imread('Images/Section2.1 - Q3/snowglobe_001.png', 0)  # load in grayscale
snowglobe2 = cv2.imread('Images/Section2.1 - Q3/snowglobe_002.png', 0)  # load in grayscale
snowglobe3 = cv2.imread('Images/Section2.1 - Q3/snowglobe_003.png', 0)  # load in grayscale
snowglobe4 = cv2.imread('Images/Section2.1 - Q3/snowglobe_004.png', 0)  # load in grayscale

output_image = average_images([snowglobe1, snowglobe2, snowglobe3, snowglobe4])
cv2.imwrite('Images/Section2.1 - Q3/output.png', output_image)


# Section 2.2 Question 1
def contrast_stretch(in_img):
    ret_img = in_img.copy()
    height = ret_img.shape[0]
    width = ret_img.shape[1]
    r_max = ret_img.max()
    r_min = ret_img.min()

    for row in range(height):
        for col in range(width):
            ret_img[row][col] = keep_8_bits(
                np.round(
                    255 * ((1 / (r_max - r_min)) * ret_img[row][col] - (r_min / (r_max - r_min)))
                )
            )
    return ret_img


input_image = cv2.imread('Images/Section2.2 - Q1/motion01.512.tiff', 0)  # load in grayscale
output_image = contrast_stretch(input_image)
cv2.imwrite('Images/Section2.2 - Q1/motion01.512_output.png', output_image)

input_image = cv2.imread('Images/Section2.2 - Q1/183087.jpg', 0)  # load in grayscale
output_image = contrast_stretch(input_image)
cv2.imwrite('Images/Section2.2 - Q1/183087_output.png', output_image)


# Section 2.2 Question 2
def contrast_piecewise(in_img, a, b):
    ret_img = in_img.copy()
    height = ret_img.shape[0]
    width = ret_img.shape[1]
    r_1 = a[0]
    s_1 = a[1]
    r_2 = b[0]
    s_2 = b[1]

    line_1_slope = s_1 / r_1 if r_1 else 0
    line_2_slope = (s_2 - s_1) / (r_2 - r_1) if (r_2 - r_1) else 0
    line_3_slope = (255 - s_2) / (255 - r_2) if (255 - r_2) else 0
    line_2_y_intercept = s_1 - line_2_slope * r_1
    line_3_y_intercept = s_2 - line_3_slope * r_2

    for row in range(height):
        for col in range(width):
            intensity = ret_img[row][col]
            if intensity < r_1:
                ret_img[row][col] = keep_8_bits(np.round(line_1_slope * intensity))
            elif intensity > r_2:
                ret_img[row][col] = keep_8_bits(np.round(line_3_slope * intensity + line_3_y_intercept))
            else:
                ret_img[row][col] = keep_8_bits(np.round(line_2_slope * intensity + line_2_y_intercept))
    return ret_img


input_image = cv2.imread('Images/Section2.2 - Q2/7.1.01.tiff', 0)  # load in grayscale
output_image = contrast_piecewise(input_image, [5, 0], [160, 255])
cv2.imwrite('Images/Section2.2 - Q2/7.1.01_output.png', output_image)

input_image = cv2.imread('Images/Section2.2 - Q2/7.2.01.tiff', 0)  # load in grayscale
output_image = contrast_piecewise(input_image, [10, 0], [150, 255])
cv2.imwrite('Images/Section2.2 - Q2/7.2.01_output.png', output_image)


# Section 2.2 Question 3
def contrast_highlight(img, A, B, I_min):
    ret_img = img.copy()
    height = ret_img.shape[0]
    width = ret_img.shape[1]

    for row in range(height):
        for col in range(width):
            intensity = ret_img[row][col]
            if intensity < A or intensity > B:
                ret_img[row][col] = I_min
    return ret_img


input_image = cv2.imread('Images/Section2.2 - Q3/3096.jpg', 0)  # load in grayscale
output_image = contrast_highlight(input_image, 0, 80, 255)
cv2.imwrite('Images/Section2.2 - Q3/3096_output.png', output_image)

input_image = cv2.imread('Images/Section2.2 - Q3/208001.jpg', 0)  # load in grayscale
output_image = contrast_highlight(input_image, 160, 255, 0)
cv2.imwrite('Images/Section2.2 - Q3/208001_output.png', output_image)


# Section 2.2 Question 4
def contrast_tfrm_curve(img, T):
    ret_img = img.copy()
    height = ret_img.shape[0]
    width = ret_img.shape[1]

    for row in range(height):
        for col in range(width):
            intensity = ret_img[row][col]
            ret_img[row][col] = T[intensity]
    return ret_img


# Section 2.2 Question 5
def generate_lut(img):
    T = [0 for x in range(256)]
    r_max = img.max()
    r_min = img.min()

    for i in range(256):
        T[i] = keep_8_bits(
            np.round(
                255 * ((1 / (r_max - r_min)) * i - (r_min / (r_max - r_min)))
            )
        )
    return T


input_image = cv2.imread('Images/Section2.2 - Q1/motion01.512.tiff', 0)  # load in grayscale
output_image = contrast_tfrm_curve(input_image, generate_lut(input_image))
cv2.imwrite('Images/Section2.2 - Q1/motion01.512_output2.png', output_image)

input_image = cv2.imread('Images/Section2.2 - Q1/183087.jpg', 0)  # load in grayscale
output_image = contrast_tfrm_curve(input_image, generate_lut(input_image))
cv2.imwrite('Images/Section2.2 - Q1/183087_output2.png', output_image)

# def show_image_and_wait(img):
#     cv2.imshow('image window', img)
#     cv2.waitKey(0)
#     cv2.destroyAllWindows()
# https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_histograms/py_histogram_begins/py_histogram_begins.html#plotting-histograms
# from matplotlib import pyplot as plt
# plt.hist(input_image.ravel(), 256, [0, 256])
# plt.show()
